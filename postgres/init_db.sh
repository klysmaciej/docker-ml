#!/bin/bash
set -e
set -x

psql --username postgres -f /usr/local/postgres/db.sql
psql --username postgres -d log-generator-db -f /usr/local/postgres/schema.sql
