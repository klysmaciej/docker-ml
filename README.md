# Goal
Run ELK + ML in docker

# Requiremets

  - VirtualBox
  - Docker
  - Docker Compose
  - Docker Machine
  - Node.js, Bower and npm
  - Maven 3.3.9
  - Maven configured to use nexus as a repository
  - Java 8

# Usage

```
docker-machine create --driver virtualbox --virtualbox-cpu-count "2" --virtualbox-memory "1024" docker-ml
docker-machine start docker-ml
eval $(docker-machine env docker-ml)
./checkout-sources # checks out the source code in src/
./build-sources    
docker-compose build
docker-compose up -d # Remove the -d option if you want to run in the foreground
```

# Testing your changes

After you have made some changes you need to rebuild your containers with the latest changes. To do so do the following:

```
./build-sources # This will copy the new .jars to 'zipkin' directory
docker-compose build # This will rebuild the images using the new jars
docker-compose up -d # Start the containers again
```

# Trying it out

Click the following link: http://$(docker-machine ip docker-ml):8080
